import java.util.Scanner;
public class Array5 {
    public static void main(String[] args) {
        Scanner Num = new Scanner(System.in);
        int arr[] = new int[3];
        int sum = 0;

        for (int i = 0;i < arr.length;i++) {
            System.out.print("Please input arr["+ i +"]: ");
            arr[i] = Num.nextInt();
            sum = sum + arr[i];
        }
        System.out.print("arr = ");
        for (int i = 0;i < arr.length;i++) {
            System.out.print(arr[i]+ " ");
        }
        System.out.println();
        System.out.println("sum = " + sum);
    }
}
