import java.util.Scanner;
public class Array10 {
    public static void main(String[] args) {
        Scanner Num = new Scanner(System.in);
        System.out.print("Please input number of element: ");
        int N = Num.nextInt();
        int arr[] = new int[N];
        int uniall = 0;
        
        for(int i = 0;i < arr.length;i++){
            System.out.print("Element "+ i +" : ");
            int IP = Num.nextInt();
            int index = -1;
            for (int j = 0;j < uniall;j++){
                if(arr[j] == IP){
                    index = j;
                }
            }
            if (index < 0) {
                arr[uniall] = IP;
                uniall++;
            }
        }
        System.out.print("arr = ");
        for (int i = 0;i < uniall;i++){
            System.out.print(arr[i] + " ");
        }
    }
}