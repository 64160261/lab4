import java.util.Scanner;
public class Array8 {
    public static void main(String[] args) {
        Scanner Num = new Scanner(System.in);
        int arr[] = new int[3];
        int sum = 0;

        for (int i = 0;i < arr.length;i++) {
            System.out.print("Please input arr["+ i +"]: ");
            arr[i] = Num.nextInt();
            sum = sum + arr[i];
        }
        System.out.print("arr = ");
        for (int i = 0;i < arr.length;i++) {
            System.out.print(arr[i]+ " ");
        }
        double avg = ((double)sum)/arr.length;
        System.out.println();
        System.out.println("sum = " + sum);
        System.err.println("avg = " + avg);

        int min = arr[0];
        for (int i = 0;i < arr.length;i++){
            if(min > arr[i]) {
                min = arr[i];
            }
        }
        System.out.println("min = " + min);

        int max = arr[0];
        for (int i = 0;i < arr.length;i++){
            if(max < arr[i]) {
                max = arr[i];
            }
        }
        System.out.println("max = " + max);
    }
}
